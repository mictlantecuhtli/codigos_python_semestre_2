class Persona:
    def __init__(self, nombre, edad):
        self._nombre = nombre
        self._edad = edad

    # Getter para la variable nombre
    def get_nombre(self):
        return self._nombre

    # Setter para la variable nombre
    def set_nombre(self, nombre):
        self._nombre = nombre

    # Getter para la variable edad
    def get_edad(self):
        return self._edad

    # Setter para la variable edad
    def set_edad(self, edad):
        self._edad = edad

# Crear una instancia de la clase Persona
persona = Persona("Juan", 30)

# Acceder a los valores de las variables utilizando los getters
print("Nombre:", persona.get_nombre())
print("Edad:", persona.get_edad())

# Modificar los valores de las variables utilizando los setters
persona.set_nombre("Pedro")
persona.set_edad(35)

# Acceder a los nuevos valores de las variables utilizando los getters
print("Nuevo Nombre:", persona.get_nombre())
print("Nueva Edad:", persona.get_edad())
