import matplotlib.pyplot as plt

# Datos de ejemplo
datos = [
    ["Nombre", "Edad", "Género"],
    ["Juan", 25, "Masculino"],
    ["María", 30, "Femenino"],
    ["Pedro", 28, "Masculino"],
]

# Crear la figura y los ejes
fig, ax = plt.subplots()

# Ocultar ejes
ax.axis("off")

# Crear la tabla y agregar los datos
tabla = ax.table(cellText=datos, loc="center")

# Estilo de la tabla
tabla.auto_set_font_size(False)
tabla.set_fontsize(12)
tabla.scale(1.2, 1.2)  # Ajustar tamaño de la tabla

# Mostrar la figura
plt.show()

