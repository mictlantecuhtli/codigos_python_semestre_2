import tkinter as tk

def login():
    username = entry_username.get()
    password = entry_password.get()
    
    if username == "admin" and password == "admin123":
        lbl_result.config(text="Inicio de sesión exitoso", fg="green")
    else:
        lbl_result.config(text="Inicio de sesión fallido", fg="red")
        
# Crear la ventana principal
window = tk.Tk()
window.title("Interfaz de Logueo")
window.geometry("300x200")

# Etiquetas de usuario y contraseña
lbl_username = tk.Label(window, text="Usuario:")
lbl_username.pack()
entry_username = tk.Entry(window)
entry_username.pack()

lbl_password = tk.Label(window, text="Contraseña:")
lbl_password.pack()
entry_password = tk.Entry(window, show="*")
entry_password.pack()

# Botón de inicio de sesión
btn_login = tk.Button(window, text="Iniciar sesión", command=login)
btn_login.pack()

# Etiqueta de resultado
lbl_result = tk.Label(window, text="")
lbl_result.pack()

# Ejecutar la interfaz de logueo
window.mainloop()

