#!/usr/local/bin/python
# -*- coding: utf-8 -*-
  
#####################################
#Fecha: 26-03-2023                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

class Estudiante:
    def __init__ (self,nombres,apelllidos,carnet,carrera,edad):
        self.nombres = nombres
        self.apellidos = apellidos
        self.carnet = carnet
        self.carrera = carrera
        self.edad = edad


    # Getters
    def get_nombres(self):
        return self._nombres

    def get_apellidos(self):
        return self._apellidos

    def get_carnet(self):
        return self._carnet

    def get_carrera(self):
        return self._carrera

    def get_edad(self):
        return self._edad


    # Setters
    def set_nombres(self,nombres):
        self._nombres = nombres

    def set_apellidos(self,apellidos):
        self._apellidos = apellidos

    def set_carnet(self,carnet):
        self._carnet = carnet

    def set_carrera(self,carrera):
        self._carrera = carrera

    def set_edad(self,edad):
        self._edad = edad


nombres = input("Ingrese sus nombres: ")
apellidos = input("Ingrese sus apellidos: ")
carnet = input("Ingrese el carnet: ")
carrera = input("Ingrese su carrera: ")
edad = int(input("Ingrese su edad: "))

estudiante = Estudiante(nombres,apellidos,carnet,carrera,edad)

estudiante.set_nombres(nombres)
estudiante.set_apellidos(apellidos)
estudiante.set_carnet(carnet)
estudiante.set_carrera(carrera)
estudiante.set_edad(edad)

print("")
print("Nombres: " , estudiante.get_nombres())
print("Apellidos: " , estudiante.get_apellidos())
print("Carnet: " , estudiante.get_carnet())
print("Carrera: " , estudiante.get_carrera())
print("Edad: " , estudiante.get_edad())
