from tabulate import tabulate

# Valor a imprimir en forma de tabla
valor = 123.456

# Encabezados de la tabla
encabezados = ['Valor']

# Datos de la tabla
datos = [[valor]]

# Imprimir la tabla
print(tabulate(datos, headers=encabezados, tablefmt='fancy_grid'))

