class Persona:
    def __init__(self, nombre, edad):
        self._nombre = nombre
        self._edad = edad

    @property
    def nombre(self):
        return self._nombre

    @nombre.setter
    def nombre(self, valor):
        self._nombre = valor

    @property
    def edad(self):
        return self._edad

    @edad.setter
    def edad(self, valor):
        if valor < 0:
            raise ValueError("La edad no puede ser negativa")
        self._edad = valor

# Crear una instancia de la clase Persona
persona = Persona("Juan", 30)

# Acceder a los atributos
print(persona.nombre)  # Output: Juan
print(persona.edad)    # Output: 30

# Modificar los atributos utilizando los setters
persona.nombre = "Carlos"
persona.edad = 35

# Acceder a los atributos actualizados
print(persona.nombre)  # Output: Carlos
print(persona.edad)    # Output: 35

