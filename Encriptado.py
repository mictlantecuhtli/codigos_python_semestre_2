# Función para encriptar un mensaje
def encriptar(mensaje, clave):
    mensaje_encriptado = ""
    for caracter in mensaje:
        if caracter.isalpha():  # Solo encriptamos letras
            mayuscula = False
            if caracter.isupper():
                mayuscula = True
            ascii_inicial = ord('a') if caracter.islower() else ord('A')
            indice = (ord(caracter) - ascii_inicial + clave) % 26
            caracter_encriptado = chr(indice + ascii_inicial)
            if mayuscula:
                caracter_encriptado = caracter_encriptado.upper()
            mensaje_encriptado += caracter_encriptado
        else:
            mensaje_encriptado += caracter
    return mensaje_encriptado

# Función para desencriptar un mensaje
def desencriptar(mensaje_encriptado, clave):
    return encriptar(mensaje_encriptado, 26 - clave)  # Desencriptar es encriptar con la clave complementaria

# Ejemplo de uso
mensaje = input("Ingrese el mensaje a encriptar: ")
clave = int(input("Ingrese la clave de encriptación: "))

print("\nMensaje original:", mensaje)
mensaje_encriptado = encriptar(mensaje, clave)
print("Mensaje encriptado:", mensaje_encriptado)
mensaje_desencriptado = desencriptar(mensaje_encriptado, clave)
print("Mensaje desencriptado:", mensaje_desencriptado)
