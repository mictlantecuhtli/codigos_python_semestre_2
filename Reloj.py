import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from datetime import datetime

# Configuración del reloj
fig, ax = plt.subplots()
ax.set_aspect('equal')
ax.set_xlim(-1.2, 1.2)
ax.set_ylim(-1.2, 1.2)
ax.axis('off')

# Función para crear una matriz de transformación de rotación
def get_rotation_matrix(angle):
    cos_theta = np.cos(angle)
    sin_theta = np.sin(angle)
    return np.array([[cos_theta, -sin_theta], [sin_theta, cos_theta]])

# Función de animación que actualiza la posición de las manecillas del reloj
def animate(frame):
    ax.clear()
    ax.set_aspect('equal')
    ax.set_xlim(-1.2, 1.2)
    ax.set_ylim(-1.2, 1.2)
    ax.axis('off')
    
    current_time = datetime.now()
    hour = current_time.hour % 12
    minute = current_time.minute
    second = current_time.second
    
    # Calcula las matrices de transformación para las manecillas del reloj
    hour_matrix = get_rotation_matrix((hour + minute / 60) * (2 * np.pi / 12))
    minute_matrix = get_rotation_matrix((minute + second / 60) * (2 * np.pi / 60))
    second_matrix = get_rotation_matrix(second * (2 * np.pi / 60))
    
    # Dibuja las manecillas del reloj
    hour_hand = np.array([[0, 0.4], [0, 0]])
    minute_hand = np.array([[0, 0.6], [0, 0]])
    second_hand = np.array([[0, 0.8], [0, 0]])
    
    transformed_hour_hand = np.dot(hour_matrix, hour_hand)
    transformed_minute_hand = np.dot(minute_matrix, minute_hand)
    transformed_second_hand = np.dot(second_matrix, second_hand)
    
    ax.plot(transformed_hour_hand[0], transformed_hour_hand[1], color='black', linewidth=4)
    ax.plot(transformed_minute_hand[0], transformed_minute_hand[1], color='black', linewidth=3)
    ax.plot(transformed_second_hand[0], transformed_second_hand[1], color='red', linewidth=2)

# Crea la animación
ani = animation.FuncAnimation(fig, animate, frames=360, interval=1000)
plt.show()

