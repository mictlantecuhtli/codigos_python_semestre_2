import matplotlib.pyplot as plt

# Datos para la gráfica
x = [1, 2, 3, 4, 5]
y = [2, 4, 6, 8, 10]

# Crear la gráfica
plt.plot(x, y)

# Personalizar la gráfica
plt.xlabel('Eje x')
plt.ylabel('Eje y')
plt.title('Gráfica simple')

# Mostrar la gráfica
plt.show()

