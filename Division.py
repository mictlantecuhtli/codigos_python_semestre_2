#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys

#####################################
#Fecha: 22-03-2023                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

def cociente ( numerador , denominador ):
    resultado = (numerador / denominador)
    print("Numerador: " , numerador , " Denominador: " , denominador , " Resultado: " , resultado)

numerador = int(input("Introduzca un numerador entero: "))
denominador = int(input("Introduzca un denominador entero: "))

cociente(numerador,denominador)
