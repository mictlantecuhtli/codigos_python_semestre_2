#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys
  
  #####################################
  #Fecha: 26-03-2023                  #
  #Autor: Fatima Azucena MC           #
  #<fatimaazucenamartinez274@gmail.com#
  #####################################

class Inventario: 
    def __init__( self, nombre, cantidad):
        self.nombre = nombre
        self.cantidad = cantidad

    # Getter para referencia
    def get_referencia(self):
        return self._nombre

    # Getter para cantidad
    def get_cantidad(self):
        return self._cantidad

    # Setter  para referencia
    def set_referencia(self, nombre):
        self._nombre = nombre

    # Setter para cantidad
    def set_cantidad(self,cantidad):
        self._cantidad = cantidad

nombre = input("Ingrese la referencia del producto: ")
cantidad = int(input("Ingrese la cantidad: "))

inventario = Inventario(nombre,cantidad)
inventario.set_referencia(nombre)
inventario.set_cantidad(cantidad)

print("Referencia: " , inventario.get_referencia())
print("Cantidad: ", inventario.get_cantidad())


