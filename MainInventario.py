#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys
  
#####################################
#Fecha: 26-03-2023                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

class Inventario:
    def __init__(self, referenciaP, cantidad):
        self._referenciaP = referenciaP
        self._cantidad = cantidad

    @property
    def referenciaP(self):
        return self._referenciaP

    @referenciaP.setter
    def referenciaP(self,referencia)
        self._referenciaP = referencia

    @property
    def cantidad(self):
        return self._cantidad

    @cantidad.setter
    def cantidad(self, canti):
        self._cantidad = canti


referenciaP = input("Ingrese la referencia del producto: ")
cantidad = int(input("Ingrese la cantidad de producto: "))

inventario = Inventario(referenciaP,cantidad)

