import matplotlib.pyplot as plt

# Datos de los puntos
x = [1, 2, 3, 4, 5]
y = [2, 4, 6, 8, 10]

# Graficar la línea
plt.plot(x, y, color='blue', linewidth=2, label='Línea')

# Graficar los puntos
plt.scatter(x, y, color='red', marker='o', label='Puntos')

# Añadir etiquetas y título
plt.xlabel('Eje X')
plt.ylabel('Eje Y')
plt.title('Gráfica Lineal con Puntos')

# Mostrar leyenda
plt.legend()

# Mostrar la gráfica
plt.show()

