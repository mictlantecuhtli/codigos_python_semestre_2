import matplotlib.pyplot as plt

# Datos de ejemplo
x = [1, 2, 3, 4, 5]
y = [2, 4, 6, 8, 10]

# Crear la figura y los ejes
fig, ax = plt.subplots()

# Graficar la gráfica lineal
ax.plot(x, y)

# Agregar etiquetas y título
ax.set_xlabel('Eje X')
ax.set_ylabel('Eje Y')
ax.set_title('Gráfica Lineal')

# Mostrar la gráfica
plt.show()

