#!/usr/local/bin/python
# -*- coding: utf-8 -*-

#####################################
#Fecha: 26-03-2023                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

# Librerías
import numpy as np
import math
import matplotlib.pyplot as plt
import pandas as pd
import colorama
from tabulate import tabulate

# Arreglos
Totales = []
Promedio = []

colorama.init()

# Litros ( 80 datos )
litros = np.array([0.09,2.00,0.97,5.60,0.15,0.25,8.20,24.00,
                   0.10,27.00,0.05,2.04,45.00,0.13,2.45,31.00,
                   10.00,7.50,3.04,1.03,5.73,18.00,13.00,8.72,
                   2.10,7.04,0.86,0.07,4.00,1.02,2.83,3.82,8.32,
                   0.72,0.02,0.65,0.04,3.00,7.20,0.24,41.40,38.90,
                   0.07,0.72,63.00,0.14,15.00,21.30,36.30,21.00,
                   14.00,76.50,12.00,28.00,2.60,16.00,4.16,6.50,3.20,
                   2.40,3.25,1.90,6.30,1.19,4.65,2.01,10.70,24.35,
                   24.90,27.00,8.10,0.87,0.26,30.00,32.30,39.90,
                   45.66,33.89,15.34,43.87])

# Kilometros ( 80 datos )
kilometros = np.array([1.12,24.00,11.58,67.20,1.85,2.98,98.40,288.00,
                       1.21,324.00,0.60,24.48,540.00,1.50,29.40,372.00,
                       120.00,90.00,36.48,12.36,68.76,216.00,156.00,
                       104.64,25.20,84.48,10.32,0.86,48.00,12.24,33.96,
                       45.84,99.84,8.64,0.18,7.80,0.48,36.00,86.40,2.88,
                       496.80,466.80,0.84,8.64,756.00,1.68,180.00,255.60,
                       435.60,252.00,168.00,918.00,144.00,336.00,31.20,
                       192.00,49.92,78.00,38.40,28.80,39.00,22.80,75.60,
                       14.28,55.80,24.12,128.40,292.20,298.80,331.20,
                       97.20,10.46,3.14,360.00,387.00,478.80,547.92,
                       406.68,184.08,526.44])

# Litros ( 20  datos )
litros2 = np.array([71.02,86.20,24.20,27.70,77.20,94.00,82.00,
                    18.80,15.41,85.17,21.40,31.34,45.98,0.65,
                    98.76,0.65,20.00,0.23,64.00,0.34])

# Imprimé los números con punto decimal normal, sin notación cientifíca
np.set_printoptions(suppress=True)

# Datos de arreglo litros y kilometros elevado al cuadrado
litrosCuadrados = np.array(np.square(litros))
kilometrosCuadrados = np.array(np.square(kilometros))

# x * y
kilometrosLitros = (litros * kilometros)

# Totales
sumaK = np.sum(kilometros)
sumaL = np.sum(litros)
sumaKC = np.sum(kilometrosCuadrados)
sumaLC = np.sum(litrosCuadrados)
sumaKL = np.sum(kilometrosLitros)

# Promedio
promedioK = (sumaK / 80)
promedioL = (sumaL / 80)
promedioKC = (sumaKC / 80)
promedioLC = (sumaLC / 80)
promedioKL = (sumaKL / 80)

# Agregar los totales a Totales para volverlo arreglo
Totales.append(sumaL)
Totales.append(sumaK)
Totales.append(sumaLC)
Totales.append(sumaKC)
Totales.append(sumaKL)

# Agregar los promedios a Promedio para volverlo arreglo
Promedio.append(promedioL)
Promedio.append(promedioK)
Promedio.append(promedioLC)
Promedio.append(promedioKC)
Promedio.append(promedioKL)


Sx = (promedioLC - (promedioL ** 2))
Sy = (promedioKC - (promedioK ** 2))
Sxy = (promedioKL - (promedioL*promedioK))
r = (Sxy/(math.sqrt(Sx)*(math.sqrt(Sy))))
rC = (r ** 2)
b = (Sxy / Sx)
a = (promedioK-(b*promedioL))
y = (a+b)

litrosY = (b*(np.array(litros2)))

print("")

encabezados = ['No.','Litros','Kilometros','x','y','x*y']
data = {
    'Litros': np.array(litros),
    'Kilometros': np.array(kilometros),
    'x^2': np.array(litrosCuadrados),
    'y^2': np.array(kilometrosCuadrados),
    'x*y': np.array(kilometrosLitros)
}

df = pd.DataFrame(data)
#print(df)
print(tabulate(df, headers= encabezados, tablefmt='fancy_grid'))
print("")

encabezados1 = ['Total','Promedio']
data1 = {
    'Total': np.array(Totales),
    'Promedio': np.array(Promedio)
}

df1 = pd.DataFrame(data1)
#print(df1)
print(tabulate(df1, headers=encabezados1, tablefmt='fancy_grid'))
print("")

encabezados2 = ['Litros','Litros*y']
data2 = {
        'Litros': np.array(litros2),
        'Litros*y': np.array(litrosY)
}

df2 = pd.DataFrame(data2)
#print(df2)
print(tabulate(df2, headers=encabezados2, tablefmt='fancy_grid'))
print("")

# Graficar la linea
plt.plot(litros2,litrosY, color='blue' , linewidth=2)

#Grafica los puntos
plt.scatter(litros2,litrosY, color='red', marker='o')

# Mostrar la gráfica
plt.show()

# Graficar la linea
plt.plot(litros,kilometros, color='blue' , linewidth=2)

#Grafica los puntos
plt.scatter(litros,kilometros, color='red', marker='o')

# Añadir las etiquetas
plt.xlabel("Litros")
plt.ylabel("Kilometros")

# Mostrar la gráfica
plt.show()

