import pandas as pd
import matplotlib.pyplot as plt

# Datos de ejemplo
data = {
    'Año': [2018, 2019, 2020, 2021, 2022],
    'Ventas': [1000, 1500, 1200, 1800, 2000]
}

# Crear un DataFrame a partir de los datos
df = pd.DataFrame(data)

# Graficar la tabla
plt.plot(df['Año'], df['Ventas'], marker='o')

# Personalizar la gráfica
plt.xlabel('Año')
plt.ylabel('Ventas')
plt.title('Ventas por Año')

# Mostrar la gráfica
plt.show()

