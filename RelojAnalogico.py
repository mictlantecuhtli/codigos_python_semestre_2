import pygame
import math
from pygame.locals import *

# Inicializar Pygame
pygame.init()

# Configuración de la ventana
width, height = 800, 600
screen = pygame.display.set_mode((width, height))
pygame.display.set_caption("Reloj Analógico")

# Definir colores
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)

# Configuración del reloj
clock_radius = min(width, height) // 2 - 50
center_x, center_y = width // 2, height // 2

# Función para dibujar el reloj
def draw_clock():
    # Dibujar el fondo
    screen.fill(BLACK)
    
    # Obtener la hora actual
    current_time = pygame.time.get_ticks() // 1000  # Segundos desde el inicio de Pygame
    
    # Dibujar el círculo del reloj
    pygame.draw.circle(screen, WHITE, (center_x, center_y), clock_radius, 5)
    
    # Dibujar los números en el reloj
    for i in range(1, 13):
        angle = math.radians(i * 30 - 60)
        num_x = center_x + int(0.8 * clock_radius * math.cos(angle))
        num_y = center_y + int(0.8 * clock_radius * math.sin(angle))
        number_surface = pygame.font.Font(None, 36).render(str(i), True, WHITE)
        number_rect = number_surface.get_rect(center=(num_x, num_y))
        screen.blit(number_surface, number_rect)
    
    # Dibujar las agujas del reloj
    second_angle = math.radians(current_time % 60 * 6 - 90)
    minute_angle = math.radians(current_time // 60 % 60 * 6 - 90)
    hour_angle = math.radians(current_time // 3600 % 12 * 30 - 90)
    
    # Agujas de segundos
    pygame.draw.line(screen, WHITE, (center_x, center_y),
                     (center_x + int(0.9 * clock_radius * math.cos(second_angle)),
                      center_y + int(0.9 * clock_radius * math.sin(second_angle))), 2)
    
    # Agujas de minutos
    pygame.draw.line(screen, WHITE, (center_x, center_y),
                     (center_x + int(0.7 * clock_radius * math.cos(minute_angle)),
                      center_y + int(0.7 * clock_radius * math.sin(minute_angle))), 4)
    
    # Agujas de horas
    pygame.draw.line(screen, WHITE, (center_x, center_y),
                     (center_x + int(0.5 * clock_radius * math.cos(hour_angle)),
                      center_y + int(0.5 * clock_radius * math.sin(hour_angle))), 6)

# Bucle principal del juego
running = True
while running:
    for event in pygame.event.get():
        if event.type == QUIT:
            running = False
    
    # Dibujar el reloj en cada iteración del bucle
    draw_clock()
    
    # Actualizar la pantalla
    pygame.display.flip()

# Salir de Pygame
pygame.quit()

